#!/bin/bash

# Formats, creates ext4 partition and LUKS encrypts the storage device
format_device() {
    local device=$1

    # List all block devices
    lsblk

    # Confirm the format operation
    read -p "Are you sure you want to format $device? Type 'yes' to confirm: " confirmation
    if [ "$confirmation" != "yes" ]; then
        echo "Aborting format operation."
        exit 1
    fi

    # Unmount any mounted partitions
    for part in $(lsblk -ln -o MOUNTPOINT "$device" | grep -v '^$'); do
        echo "Unmounting $part..."
        sudo umount "$part"
        if [ $? -ne 0 ]; then
            echo "Failed to unmount $part."
            exit 1
        fi
    done

    # Remove existing encrypted partition if it exists
    if [ -e /dev/mapper/encrypted_device ]; then
        echo "Removing existing encrypted partition..."
        sudo cryptsetup close encrypted_device
        if [ $? -ne 0 ]; then
            echo "Failed to remove existing encrypted partition."
            exit 1
        fi
    fi

    # Create a new partition table
    echo "Creating new partition table on $device..."
    sudo parted "$device" mklabel gpt
    if [ $? -ne 0 ]; then
        echo "Failed to create partition table on $device."
        exit 1
    fi

    # Create a partition
    echo "Creating a new partition on $device..."
    sudo parted -a optimal "$device" mkpart primary 0% 100%
    if [ $? -ne 0 ]; then
        echo "Failed to create partition on $device."
        exit 1
    fi

    # Encrypt the partition
    echo "Encrypting the partition..."
    sudo cryptsetup luksFormat "${device}1"
    if [ $? -ne 0 ]; then
        echo "Failed to encrypt the partition."
        exit 1
    fi

    # Open the encrypted partition
    echo "Opening the encrypted partition..."
    sudo cryptsetup open "${device}1" encrypted_device
    if [ $? -ne 0 ]; then
        echo "Failed to open the encrypted partition."
        exit 1
    fi

    # Prompt for filesystem label
    read -p "Enter label for the ext4 filesystem: " label
    if [ -z "$label" ]; then
        echo "Label cannot be empty."
        exit 1
    fi

    # Create ext4 filesystem
    echo "Creating ext4 filesystem on the encrypted partition..."
    sudo mkfs.ext4 -L "$label" /dev/mapper/encrypted_device
    if [ $? -ne 0 ]; then
        echo "Failed to create filesystem."
        exit 1
    fi

    # Close the encrypted partition
    echo "Closing the encrypted partition..."
    sudo cryptsetup close encrypted_device
    if [ $? -ne 0 ]; then
        echo "Failed to close the encrypted partition."
        exit 1
    fi

    echo "Storage device $device has been successfully formatted and encrypted."
}

# Open the encrypted storage device and mount at /run/mount/device/label
open_device() {
    local device=$1

    # Open the encrypted partition
    echo "Opening the encrypted partition on $device..."
    sudo cryptsetup open "${device}1" encrypted_device
    if [ $? -ne 0 ]; then
        echo "Failed to open the encrypted partition."
        exit 1
    fi

    # Create mount point if it doesn't exist
    local label=$(sudo blkid -o value -s LABEL /dev/mapper/encrypted_device)
    local mount_point="/run/mount/device/$label"
    if [ ! -d "$mount_point" ]; then
        echo "Creating mount point $mount_point..."
        sudo mkdir -p "$mount_point"
        if [ $? -ne 0 ]; then
            echo "Failed to create mount point $mount_point."
            exit 1
        fi
    fi

    # Mount the encrypted partition
    echo "Mounting the encrypted partition..."
    sudo mount /dev/mapper/encrypted_device "$mount_point"
    if [ $? -ne 0 ]; then
        echo "Failed to mount the encrypted partition."
        exit 1
    fi

    # Change ownership to the original user and root group
    local original_user=$(logname)
    echo "Changing ownership of $mount_point to $original_user:root..."
    sudo chown "$original_user:root" "$mount_point"
    if [ $? -ne 0 ]; then
        echo "Failed to change ownership of $mount_point."
        exit 1
    fi

    echo "Encrypted storage device $device has been successfully opened and mounted at $mount_point."
}

# Unmount and close the encrypted storage device
close_device() {
    local label=$(sudo blkid -o value -s LABEL /dev/mapper/encrypted_device)
    local mount_point="/run/mount/device/$label"

    echo "Unmounting $mount_point..."
    sudo umount "$mount_point"
    if [ $? -ne 0 ]; then
        echo "Failed to unmount $mount_point."
        exit 1
    fi

    echo "Closing the encrypted partition..."
    sudo cryptsetup close encrypted_device
    if [ $? -ne 0 ]; then
        echo "Failed to close the encrypted partition."
        exit 1
    fi

    echo "Encrypted storage device has been successfully closed."
}

# Wipe the storage device
wipe_device() {
    local device=$1

    lsblk

    # Confirm the wipe operation
    read -p "Are you sure you want to wipe $device? Type 'yes' to confirm: " confirmation
    if [ "$confirmation" != "yes" ]; then
        echo "Aborting wipe operation."
        exit 1
    fi

    echo "Wiping $device..."
    sudo dd if=/dev/zero of="$device" bs=4M status=progress
    if [ $? -ne 0 ]; then
        echo "Failed to wipe $device."
        exit 1
    fi

    echo "Storage device $device has been successfully wiped."
}

# Display help menu
show_help() {
    echo "Usage: $0 -f|-o|-c|-w /dev/sdX"
    echo "  -f: Format to ext4 and encrypt the storage device"
    echo "  -o: Open and mount the encrypted storage device at /run/mount/device/[device_label]"
    echo "  -c: Unmount and close the encrypted storage device"
    echo "  -w: Wipe the storage device"
}

if [ "$#" -lt 2 ]; then
    show_help
    exit 1
fi

flag=$1
device=$2

case $flag in
    -f)
        format_device "$device"
        ;;
    -o)
        open_device "$device"
        ;;
    -c)
        close_device
        ;;
    -w)
        wipe_device "$device"
        ;;
    *)
        show_help
        exit 1
        ;;
esac
